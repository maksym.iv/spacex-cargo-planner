const getRequiredBays = (boxes) => {
  if (boxes) {
    const boxesNumber = boxes.split(",").length;
    return Math.round(boxesNumber / 2);
  }
  return 0;
};

const getName = (name) => name.trim().toLowerCase();

export { getRequiredBays, getName };
