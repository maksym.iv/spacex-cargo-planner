import axios from "axios";

export const asyncTimeout = (ms) => {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
};

const link =
  "https://bitbucket.org/hpstore/spacex-cargo-planner/raw/204125d74487b1423bbf0453f4dcb53a2161353b/shipments.json";

const getShipmentsList = async () => {
  // mock time interval to display loader
  await asyncTimeout(2000);

  return await axios.get(link);
};

export default getShipmentsList;
