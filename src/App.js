import React, { useEffect, useContext } from "react";
import { AppContext } from './context';
import { SideBar, ItemDetails, Header } from "./components";
import getShipmentsList from "./api/shipments-api.js";
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import styles from "./styles/ItemDetails.module.css";

const EmptyPage = () => (
  <div className={styles.ItemDetails}>
    <p className={styles.notFound}>Choose company, please</p>
  </div>
)

export const App = () => {
  const { setCargoList, setLoading, setError } = useContext(AppContext);

  useEffect(() => {
    setLoading(true);
    getShipmentsList()
      .then(({ data }) => setCargoList(data))
      .catch(() => {
        setError("Connection error, try again later, please!");
      })
      .finally(() => setLoading(false))
  }, [setCargoList, setLoading, setError]);

  return (
    <Router>
      <Header />
      <main>
        <SideBar />
        <Switch>
          <Route path="/:id" children={<ItemDetails />} />
          <Route path="/" children={<EmptyPage />} />
        </Switch>
      </main>
    </Router>
  );
}