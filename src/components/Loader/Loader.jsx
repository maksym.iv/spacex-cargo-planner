import React from "react";
import loader from "../../assets/loader.gif";

const styles = {
  textAlign: "center",
  width: 200,
  height: 200,
};

const Loader = () => <img style={styles} src={loader} alt="loader" />;

export default Loader;
