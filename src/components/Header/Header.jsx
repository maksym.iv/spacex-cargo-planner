import React, { useCallback, useContext, useMemo, useState } from "react";
import { useHistory } from "react-router-dom";
import PropTypes from "prop-types";
import styles from "../../styles/Header.module.css";
import logo from "../../assets/logo.png";
import { getName } from "../../utils";
import { AppContext } from "../../context";

const getBtnStyles = (isOpen) => {
  const { btn, openedMenu, closedMenu } = styles;
  const status = isOpen ? closedMenu : openedMenu;
  return `${btn} ${status}`;
};

const Header = () => {
  const [searchedName, setSearchedName] = useState("");
  const history = useHistory();

  const { cargoList, isMenuOpen, menuHandler } = useContext(AppContext);

  const searchHandler = (e) => setSearchedName(e.target.value);

  const nameFind = getName(searchedName);

  const filteredList = useMemo(
    () => cargoList.filter(({ name }) => getName(name).includes(nameFind)),
    [nameFind, cargoList],
  );

  const onItemClick = useCallback(
    (id) => () => {
      history.push(id);
      setSearchedName("");
    },
    [history],
  );

  return (
    <header className={styles.Header}>
      <div className={styles.logo}>
        <img alt="logo" src={logo} />
      </div>

      <div className={styles.navigation}>
        <button className={getBtnStyles(isMenuOpen)} onClick={menuHandler} />
      </div>

      <div className={styles.search}>
        <div className={styles.searchWrapper}>
          <input
            type="text"
            placeholder="Search"
            value={searchedName}
            onChange={searchHandler}
          />
          <div className={styles.result}>
            {searchedName &&
              filteredList.map(({ name, id }) => (
                <button key={id} onClick={onItemClick(id)}>
                  {name}
                </button>
              ))}
          </div>
        </div>
      </div>
    </header>
  );
};

Header.propTypes = {
  isOpen: PropTypes.bool,
  clickHandler: PropTypes.func,
  searchHandler: PropTypes.func,
};

export default Header;
