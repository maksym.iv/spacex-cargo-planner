import React, { useContext, useMemo } from "react";
import styles from "../../styles/ItemDetails.module.css";
import Loader from "../Loader/Loader";
import { getRequiredBays } from "../../utils";
import { AppContext } from "../../context";
import { useParams } from "react-router-dom";

const ItemDetails = () => {
  const { cargoList, isLoading } = useContext(AppContext);
  const { id: currentId } = useParams();

  const item = useMemo(
    () => cargoList.find(({ id }) => id === currentId),
    [cargoList, currentId],
  );

  const requiredBays = getRequiredBays(item?.boxes);

  const renderDetails = () => {
    if (isLoading) {
      return (
        <div className={styles.loader}>
          <Loader />
        </div>
      );
    }

    if (item?.id) {
      const { name, email, boxes } = item;

      return (
        <>
          <p className={styles.name}>{name}</p>
          <p className={styles.email}>{email}</p>
          <p className={styles.cargoSubTitle}>Cargo boxes</p>
          <p className={styles.boxes}>{boxes}</p>
          <p className={styles.requireSdubTitle}>
            Number of required cargo bays
          </p>
          <p className={styles.requiredBays}>{requiredBays}</p>
        </>
      );
    }

    return <p className={styles.notFound}>Choose company, please</p>;
  };

  return <div className={styles.ItemDetails}>{renderDetails()}</div>;
};

export default ItemDetails;
