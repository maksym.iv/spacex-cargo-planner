import React, { useContext } from "react";
import { NavLink } from "react-router-dom";
import PropTypes from "prop-types";
import styles from "../../styles/SideBar.module.css";
import { Loader } from "../../components";
import logo from "../../assets/logo.png";
import { AppContext } from "../../context";

const getContainerStyle = (isOpenMenu) => {
  const { SideBar, open, close } = styles;
  const status = isOpenMenu ? open : close;

  return `${SideBar} ${status}`;
};

const getLinkStyle = (isActive) => (isActive ? { color: "#595e6d" } : {});

const SideBar = () => {
  const { cargoList, isLoading, isMenuOpen, menuHandler, error } =
    useContext(AppContext);

  return (
    <div className={getContainerStyle(isMenuOpen)}>
      <div className={styles.navigation}>
        <img className={styles.logo} alt="logo" src={logo} />
        <button className={styles.closeBtn} onClick={menuHandler} />
      </div>

      <h3>Shipment List</h3>
      <div className={styles.list}>
        {isLoading ? (
          <Loader />
        ) : (
          cargoList.map(({ name, id }) => (
            <NavLink to={`/${id}`} key={id} style={getLinkStyle}>
              {name}
            </NavLink>
          ))
        )}

        <p>{!isLoading && error}</p>
      </div>
    </div>
  );
};

SideBar.propTypes = {
  list: PropTypes.array,
  load: PropTypes.bool,
  clickHandler: PropTypes.func,
  menuHandler: PropTypes.func,
  searchedName: PropTypes.string,
  isOpenMenu: PropTypes.bool,
  error: PropTypes.string,
  currentId: PropTypes.string,
};

SideBar.defaultProps = {
  list: [],
};

export default SideBar;
