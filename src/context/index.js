import React, { useState } from "react";

export const AppContext = React.createContext();

export const Provider = AppContext.Provider;

const AppProvider = (props) => {
  const [cargoList, setCargoList] = useState([]);
  const [isLoading, setLoading] = useState(false);
  const [isMenuOpen, setIsMenuOpen] = useState(false);
  const [currentId, setCurrentId] = useState(null);
  const [error, setError] = useState(null);

  const menuHandler = () => setIsMenuOpen((prevState) => !prevState);

  return (
    <Provider
      value={{
        cargoList,
        setCargoList,
        isLoading,
        setLoading,
        isMenuOpen,
        menuHandler,
        currentId,
        setCurrentId,
        error,
        setError,
      }}
    >
      {props.children}
    </Provider>
  );
};

export default AppProvider;
